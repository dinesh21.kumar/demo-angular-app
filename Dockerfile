### STAGE 1: Build ###

FROM trion/ng-cli-karma:6.2.5 as builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

COPY . .

## Build the angular app in production mode and store the artifacts in dist folder

RUN ng build
RUN ls dist/demo-angular-app


### STAGE 2: Create NGINX container, ports 80 & 443 already exposed, and serve compiled files ###

FROM nginx:1.14.1-alpine

## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/dist/demo-angular-app /usr/share/nginx/html

CMD ["nginx", "-g", "daemon off;"]
