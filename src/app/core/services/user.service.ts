import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../environments/environment'

@Injectable()
export class UserService {
  //apiUrl = 'https://api.github.com/users';
  userListUrl = environment.baseUrl + "/users/list";

  constructor(private http: HttpClient) {}

  getUsers() {
    return this.http.get(this.userListUrl);
  }

  getUser(username: string) {
    return this.http.get(environment.baseUrl + "/users/"+username); 
  }
}
